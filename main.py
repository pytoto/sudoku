
from clingon import clingon
import sys
import time

from parts import Constants as C
from parts.io import read_input_stream, string_output, color_output
from parts.solver import Solver

clingon.DEBUG = C.DEBUG

if __name__ == '__main__':
    @clingon.clize
    def runner(file='', size=3, maximum=3, output='color'):
        """
        *****************
        * Sudoku solver *
        *****************
        - file: file name containing grid. If missing, input from stdin
        - size: grid size. Allowed values are 2, 3, 4. defaults to 3 (classic)
        - maximum: max number of results
        - output: 'color': color output (default)
                  'newline': text output with newlines
                  'raw': raw string output
        """
        try:
            color = output.startswith('color') and output.split(':')[1]
        except IndexError:
            color = 'green'
        newline = (output == 'newline')
        start = time.time()
        file_obj = open(file, 'r') if file else sys.stdin
        if size != 3:
            C.set_grid_size(size)
        places = read_input_stream(file_obj.read(), C.get_symbols_dict(), C.GRID_SURFACE)
        solver = Solver(places, maximum, color)
        solver.solve()
        results = solver.get_results()
        duration = (time.time() - start) * 1000  # milliseconds
        printed = False
        if color:
            color = '[' + color + ']'
            try:
                from rich import print as rprint
                for res in results:
                    toto = color_output(res, C.get_reverse_symbols(), color)
                    rprint(toto)
                    print()
                printed = True
            except ImportError:
                print("If you want colorful results, please 'pip install rich'")
                newline = True
        if not printed:
            for res in results:
                print(string_output(res, C.get_reverse_symbols(), newline))
                print()
        print(f"{len(results)} solution(s) found in {duration} ms:\n")
