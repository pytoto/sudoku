
# Tests are in tests/test_io.py

class NonconsistentGrid(Exception):
    """ Data exception: the grid is inconsistent: same value in row/col/square """


class PropagationException(Exception):
    """ Solver exception: this branch has no solution """


class Constants:
    DEBUG = True

    @classmethod
    def set_grid_size(cls, size_root, symbols=None):
        """
        set the grid size and constants
        :param size_root: the square root of the side size of the grid - must be in (2, 3, 4)
            3: classical Sudoku
            2: baby Sudoku
            4: giant Sudoku
        :param symbols: the symbols used, defaults to '1234...'
        """
        assert 2 <= size_root <= 4
        cls.GRID_SIZE_ROOT = size_root
        cls.GRID_SIZE = size_root * size_root
        cls.GRID_SURFACE = cls.GRID_SIZE * cls.GRID_SIZE
        cls.GRID_SIZE_1 = cls.GRID_SIZE - 1
        cls.GRID_SURFACE_1 = cls.GRID_SURFACE - 1
        cls.SQUARE_LINE_SURFACE = cls.GRID_SIZE * size_root
        if not symbols:
            if size_root == 2:
                symbols = '1234'
            elif size_root == 3:
                symbols = '123456789'
            else:
                symbols = '123456789ABCDEFG'
        assert len(symbols) == cls.GRID_SIZE
        cls.SYMBOLS = symbols
        cls.SYMBOLS_DICT = {c: i for i, c in enumerate(symbols)}

    @classmethod
    def get_symbols_dict(cls):
        return cls.SYMBOLS_DICT

    @classmethod
    def get_reverse_symbols(cls):
        return cls.SYMBOLS


# Classical Sudoku by default
Constants.set_grid_size(3)
