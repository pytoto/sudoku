
from . import Constants as C, PropagationException
from .helpers import indice_to_coordinates, coordinates_to_square
from .static_classes import AbstractPlaces


class VarPlaces:
    """
    Container for variable places
    Implement a node in the search tree when multiple solutions arise
    """

    def __init__(self, name=None, places=None, color=None):
        """
        with parameters -> root node
        without parameter -> secondary node (for multichoices place)
        :param name: name of the places class
        :param places: initial vector of places
        """
        self.depth = 0
        if name and places:
            self.__class__.P = AbstractPlaces.get_class(name)(places)
            self.__class__.color = color
            self.var_places = {}
            self.init_var_places()
            self.found = []

    def init_var_places(self):
        for indice, value in enumerate(self.P.places):
            if value is None:
                self.var_places[indice] = self.P.compute_allowed_values(indice)

    def __len__(self):
        return len(self.var_places)

    def get_first_place(self):
        """
        find and get the place with the smallest number of possible values
        :return: integer = indice of the place found or None if no more places
        """
        indice, mini = None, 100
        count = self.P.count
        for i, vals in self.var_places.items():
            cnt = count(vals)
            if cnt == 1:
                return i
            if cnt < mini:
                indice, mini = i, cnt
        return indice

    def add_found(self, indice, value):
        self.found.append((indice, value))

    def pop_place(self, indice):
        return self.var_places.pop(indice)

    def copy(self, indice):
        """
        create a secondary search node :
         - copy var_places except place @indice
         - copy places found so far
        :param indice: integer
        """
        new = self.__class__()
        new.depth = self.depth + 1
        new.var_places = {k: self.P.copy(v) for k, v in self.var_places.items() if k != indice}
        new.found = [(i, v) for i, v in self.found]
        return new

    def propagate(self, indice, value):
        """
        update allowed values in var_places in row, col and square
        :param indice: integer
        :param value: integer
        """
        row, col = indice_to_coordinates(indice)
        square = coordinates_to_square(row, col)
        count = self.P.count
        update = self.P.update
        for i in self.P.indices_in_row(row):
            if place := self.var_places.get(i):
                place = update(place, value)
                if not count(place):
                    raise PropagationException()
                self.var_places[i] = place
        for i in self.P.indices_in_col(col):
            if place := self.var_places.get(i):
                place = update(place, value)
                if not count(place):
                    raise PropagationException()
                self.var_places[i] = place
        for i in self.P.indices_in_square(square, row, col):
            if place := self.var_places.get(i):
                place = update(place, value)
                if not count(place):
                    raise PropagationException()
                self.var_places[i] = place
        self.found.append((indice, value))
        return self

    def get_result(self):
        """
        get the result (terminate) grid
        :return: a list of integers
        """
        places = list(self.P.places)  # take a copy
        color = self.color
        assert len(self.found) == places.count(None)
        for i, v in self.found:
            places[i] = 100 + v if color else v
        return places
