
from . import Constants as C


def check_index(index):
    assert 0 <= index <= C.GRID_SIZE_1


def check_indice(indice):
    assert 0 <= indice <= C.GRID_SURFACE_1


def indice_to_coordinates(indice):
    """
    return the indice of a coordinate
    :param indice: integer [0-80]
    :return: tuple(row, col)
    """
    if C.DEBUG:
        check_indice(indice)
    return divmod(indice, C.GRID_SIZE)


def coordinates_to_indice(row, col):
    """
    return the indice of a coordinate
    :param row: integer
    :param col: integer
    :return: integer
    """
    if C.DEBUG:
        check_index(row)
        check_index(col)
    return C.GRID_SIZE * row + col


def row_to_indices(row):
    """
    return all indices in a row
    :param row: integer
    :return: an iterator over row indices
    """
    if C.DEBUG:
        check_index(row)
    start = row * C.GRID_SIZE
    return range(start, start + C.GRID_SIZE)


def col_to_indices(col):
    """
    return all indices in a col
    :param col: integer
    :return: an iterator over col indices
    """
    if C.DEBUG:
        check_index(col)
    return range(col, col + C.GRID_SURFACE_1, C.GRID_SIZE)


def coordinates_to_square(row, col):
    return (row // C.GRID_SIZE_ROOT) * C.GRID_SIZE_ROOT + (col // C.GRID_SIZE_ROOT)


def indice_to_square(indice):
    """
    return the square that contains an indice
    :param indice: integer
    :return: integer
    """
    return coordinates_to_square(*indice_to_coordinates(indice))


def square_to_indices(square, row=-1, col=-1):
    """
    return iterator over indices in a square except for row and col
    :param square: integer
    :param row: integer, -1 if not used
    :param col: integer, -1 if not used
    :return: a list of 4, 6 or 9 integers
    """
    if C.DEBUG:
        check_index(square)
    _row = (square // C.GRID_SIZE_ROOT) * C.SQUARE_LINE_SURFACE
    _col = (square % C.GRID_SIZE_ROOT) * C.GRID_SIZE_ROOT
    row *= C.GRID_SIZE
    return ((i + j) for i in range(_row, _row + C.SQUARE_LINE_SURFACE, C.GRID_SIZE)
            for j in range(_col, _col + C.GRID_SIZE_ROOT) if i != row and j != col)
