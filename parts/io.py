
from more_itertools import ichunked

from . import Constants as C


def read_input_stream(istream, dico, length):
    """
    read and convert input stream to places integer vector
    :param istream: iterable
    :param dico: the conversion dictionary
    :param length: integer = expected length of the places vector
    :return: list of integers and None
    """
    result = []
    for c in istream:
        if c < ' ':  # ignore control chars
            continue
        try:
            result.append(dico[c])
        except KeyError:
            result.append(None)  # everything outside dico considered empty
    assert len(result) == length
    return result


def string_output(places, dico, new_line=True):
    """
    create output string from places integer vector
    :param places: list of integers from the solver
    :param dico: container translating integers to symbols
    :param new_line: if True, insert a new line every C.GRID_SIZE symbol
    :return: string
    """
    if new_line:
        return '\n'.join(' '.join(line)
            for line in ichunked((dico[p] for p in places), C.GRID_SIZE)
        )
    else:
        return ''.join(dico[p] for p in places)


def color_output(places, dico, color):
    """
    create output string for printing with Rich
    :param places: list of integers from the solver
    :param dico: container translating integers to symbols
    :param color: string
    :return: string
    """
    def f(p):
        if p >= 100:
            return color + dico[p - 100] + '[/]'
        return dico[p]
    return '\n'.join(' '.join(line)
        for line in ichunked((f(p) for p in places), C.GRID_SIZE)
    )
