
from . import Constants as C, PropagationException
from .dynamic_classes import VarPlaces


class Solver:

    def __init__(self, places, maximum, color):
        self.var_places = VarPlaces('set', places, color)
        self.maximum = maximum
        self.results = []
        self.terminate = False

    def solve(self, var_places=None):
        var_places = var_places or self.var_places
        while True:
            indice = var_places.get_first_place()
            # No more var_place to explore
            if indice is None:
                self.add_result(var_places)
                if C.DEBUG:
                    print(f"Found solution at depth {var_places.depth}")
                if len(self.results) == self.maximum:
                    self.terminate = True
                return
            values = var_places.var_places[indice]
            # the var_place has only one possible place,
            # propagate and iterate
            if var_places.P.count(values) == 1:
                var_places.pop_place(indice)
                value = var_places.P.to_int(values)
                try:
                    var_places.propagate(indice, value)
                except PropagationException:
                    if C.DEBUG:
                        print(f"Single propagation error @depth {var_places.depth}, {indice} {value}")
                    return
            # the var_place has multiple possible values
            # call solve() recursively for each value
            else:
                for val in var_places.P.iter(values):
                    if self.terminate:
                        return
                    copy = var_places.copy(indice)
                    try:
                        copy.propagate(indice, val)
                    except PropagationException:
                        if C.DEBUG:
                            print(f"Multiple propagation error @depth {var_places.depth}")
                        continue
                    self.solve(copy)
                return

    def add_result(self, var_places):
        result = var_places.get_result()
        if C.DEBUG:  # make sure that all solutions are different
            for res in self.results:
                assert res != result
        self.results.append(result)

    def get_results(self):
        return self.results
