
from functools import reduce
from operator import or_

from . import Constants as C,NonconsistentGrid
from .helpers import (row_to_indices, col_to_indices, indice_to_coordinates,
                      coordinates_to_square, square_to_indices)

"""
Classes that implement set operations necessary to calculate
variable places values
"""


ClassNameError = type('ClassNameError', (Exception,), {})


class PlaceMeta(type):
    def __new__(mcs, name, bases, dico):
        cls = super().__new__(mcs, name, bases, dico)
        if bases:
            if 'name' not in dico:
                raise ClassNameError("missing name")
            if dico['name'] in cls.places_cls:
                raise ClassNameError(f"duplicate name {dico['name']}")
            cls.places_cls[dico['name']] = cls
        return cls


class AbstractPlaces(metaclass=PlaceMeta):
    places_cls = {}

    @classmethod
    def get_class(cls, name):
        return cls.places_cls[name]

    @classmethod
    def get_names(cls):
        return tuple(cls.places_cls)

    def init_cache(self):
        self.rows = [self.values_in_row(i) for i in range(C.GRID_SIZE)]
        self.cols = [self.values_in_col(i) for i in range(C.GRID_SIZE)]
        self.squares = [self.values_in_square(i) for i in range(C.GRID_SIZE)]

    def sub_space_filled(self, indices):
        """
        return iterator over the values != None in a sub space (row, col, square)
        :param indices: iterator over the sub space
        :return: iterator of values
        """
        return (p for i in indices if (p := self._places[i]) is not None)

    def sub_space_empty(self, indices):
        """
        return iterator over the indices which values are None
        :param indices: iterator over the sub space
        :return: iterator of indices
        """
        return (i for i in indices if self.places[i] is None)

    def _values_in_row(self, row):
        return self.sub_space_filled(row_to_indices(row))

    def _values_in_col(self, col):
        return self.sub_space_filled(col_to_indices(col))

    def _values_in_square(self, square):
        return self.sub_space_filled(square_to_indices(square))

    def indices_in_row(self, row):
        return self.sub_space_empty(row_to_indices(row))

    def indices_in_col(self, col):
        return self.sub_space_empty(col_to_indices(col))

    def indices_in_square(self, square, row, col):
        return self.sub_space_empty(square_to_indices(square, row, col))


class PlacesSet(AbstractPlaces):
    name = 'set'

    def __init__(self, places):
        self.places = places
        self._places = places
        self.full_set = set(range(C.GRID_SIZE))
        self.init_cache()

    @staticmethod
    def count(value):
        return len(value)

    @staticmethod
    def iter(value):
        for val in value:
            yield val

    @staticmethod
    def from_int(*values):
        return set(values)

    @staticmethod
    def _from_int(value):
        return value

    @staticmethod
    def to_int(value):
        assert len(value) == 1
        return next(iter(value))

    @staticmethod
    def copy(value):
        return value.copy()

    @staticmethod
    def update(value, val):
        value.discard(val)
        return value

    @staticmethod
    def check_coerency(it):
        tp = tuple(it)
        ret = set(tp)
        if len(tp) != len(ret):
            raise NonconsistentGrid()
        return ret

    def values_in_row(self, row, check=True):
        it = self._values_in_row(row)
        if check:
            return self.check_coerency(it)
        return set(it)

    def values_in_col(self, col, check=True):
        it = self._values_in_col(col)
        if check:
            return self.check_coerency(it)
        return set(it)

    def values_in_square(self, square, check=True):
        it = self._values_in_square(square)
        if check:
            return self.check_coerency(it)
        return set(it)

    def compute_allowed_values(self, indice):
        row, col = indice_to_coordinates(indice)
        square = coordinates_to_square(row, col)
        return self.full_set - self.rows[row] - self.cols[col] - self.squares[square]


class PlacesInteger(AbstractPlaces):
    name = 'integer'

    def __init__(self, places):
        self.places = places
        self._places = [None if p is None else 1 << p for p in self.places]
        self.full_set = 2**C.GRID_SIZE - 1
        self.init_cache()

    @staticmethod
    def count(value):
        cnt = 0
        while value:
            cnt += value & 1
            value >>= 1
        return cnt

    @staticmethod
    def iter(value):
        cnt = 0
        while value:
            if value & 1:
                yield cnt
            cnt += 1
            value >>= 1

    @staticmethod
    def from_int(*values):
        return reduce(or_, (1 << v for v in values), 0)

    @staticmethod
    def _from_int(value):
        return 1 << value

    @staticmethod
    def to_int(value):
        assert PlacesInteger.count(value) == 1
        cnt = 0
        while True:
            if value & 1:
                return cnt
            cnt += 1
            value >>= 1

    @staticmethod
    def copy(value):
        return value

    @staticmethod
    def update(value, val):
        return value & ~(1 << val)

    @staticmethod
    def check_consistency(it):
        """
        check that the sub-space (row, col or square) does not contains duplicates
        :param it: iterable over the sub-space
        :return: the class specific representation of the values found in the sub-space
        """
        tp = tuple(it)
        ret = reduce(or_, tp, 0)
        if len(tp) != PlacesInteger.count(ret):
            raise NonconsistentGrid()
        return ret

    def values_in_row(self, row, check=True):
        it = self._values_in_row(row)
        if check:
            return self.check_consistency(it)
        return reduce(or_, it, 0)

    def values_in_col(self, col, check=True):
        it = self._values_in_col(col)
        if check:
            return self.check_consistency(it)
        return reduce(or_, it, 0)

    def values_in_square(self, square, check=True):
        it = self._values_in_square(square)
        if check:
            return self.check_consistency(it)
        return reduce(or_, it, 0)

    def compute_allowed_values(self, indice):
        row, col = indice_to_coordinates(indice)
        square = coordinates_to_square(row, col)
        return self.full_set & ~self.rows[row] & ~self.cols[col] & ~self.squares[square]
