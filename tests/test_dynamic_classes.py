
from pytest import raises

from ..parts import Constants as C, PropagationException
from ..parts.dynamic_classes import VarPlaces
from ..parts.io import read_input_stream


GRID = 'x12x3xx1xx1x1xx4'


def places_grid_4():
    C.set_grid_size(2)  # baby
    return read_input_stream(GRID, C.SYMBOLS_DICT, C.GRID_SURFACE)


def test_init():
    for kind in ('set', 'integer'):
        var_places = VarPlaces(kind, places_grid_4())
        assert len(var_places) == GRID.count('x')
        assert len(var_places) == var_places.P.places.count(None)
        assert var_places.var_places[0] == var_places.P.from_int(3)
        assert var_places.var_places[3] == var_places.P.from_int(2)
        assert var_places.var_places[5] == var_places.P.from_int(1, 3)
        assert var_places.var_places[9] == var_places.P.from_int(1, 2, 3)
        assert var_places.var_places[14] == var_places.P.from_int(2)


def test_get_first_place():
    for kind in ('set', 'integer'):
        var_places = VarPlaces(kind, places_grid_4())
        # {0: {3}, 3: {2}, 5: {1, 3}, 6: {3}, 8: {1, 3}, 9: {1, 2, 3}, 11: {1, 2}, 13: {1, 2}, 14: {2}}
        assert var_places.get_first_place() == 0
        for i in (0, 3, 6):
            del var_places.var_places[i]
        assert var_places.get_first_place() == 14
        del var_places.var_places[14]
        assert var_places.get_first_place() == 5


def test_copy_set():
    var_places = VarPlaces('set', places_grid_4())
    indice = var_places.get_first_place()
    var_places.add_found(indice, 1)
    copy = var_places.copy(indice)
    assert len(var_places.var_places) == len(copy.var_places) + 1
    assert copy.var_places.get(0) is None
    assert var_places.var_places[9] == copy.var_places[9]
    assert var_places.var_places[9] is not copy.var_places[9]
    assert var_places.found == copy.found
    assert var_places.found[0] is not copy.found[0]


def test_copy_integer():
    var_places = VarPlaces('integer', places_grid_4())
    indice = var_places.get_first_place()
    var_places.add_found(indice, 1)
    copy = var_places.copy(indice)
    assert len(var_places.var_places) == len(copy.var_places) + 1
    assert copy.var_places.get(0) is None
    assert var_places.var_places[9] == copy.var_places[9]
    assert var_places.found == copy.found
    assert var_places.found[0] is not copy.found[0]


def test_propagate():
    for kind in ('set', 'integer'):
        var_places = VarPlaces(kind, places_grid_4())
        for i in range(4):
            indice = var_places.get_first_place()
            value = var_places.P.to_int(var_places.pop_place(indice))
            var_places.propagate(indice, value)
        indice = var_places.get_first_place()
        value = var_places.P.to_int(var_places.pop_place(indice))
        with raises(PropagationException):
            var_places.propagate(indice, value)
