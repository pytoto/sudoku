
from ..parts import Constants as C
from ..parts.helpers import (indice_to_coordinates, coordinates_to_indice, indice_to_square,
                             row_to_indices, col_to_indices, square_to_indices)


def test_indice_to_coordinates():
    C.set_grid_size(3)  # classic
    assert indice_to_coordinates(0) == (0, 0)
    assert indice_to_coordinates(8) == (0, 8)
    assert indice_to_coordinates(9) == (1, 0)
    assert indice_to_coordinates(80) == (8, 8)


def test_coordinates_to_indice():
    C.set_grid_size(3)  # classic
    assert coordinates_to_indice(0, 0) == 0
    assert coordinates_to_indice(0, 8) == 8
    assert coordinates_to_indice(1, 0) == 9
    assert coordinates_to_indice(8, 8) == 80


def test_row_to_indices():
    C.set_grid_size(3)  # classic
    assert list(row_to_indices(0)) == [0, 1, 2, 3, 4, 5, 6, 7, 8]
    assert list(row_to_indices(8)) == [72, 73, 74, 75, 76, 77, 78, 79, 80]


def test_col_to_indices():
    C.set_grid_size(3)  # classic
    assert list(col_to_indices(0)) == [0, 9, 18, 27, 36, 45, 54, 63, 72]
    assert list(col_to_indices(8)) == [8, 17, 26, 35, 44, 53, 62, 71, 80]


def test_indice_to_square():
    C.set_grid_size(3)  # classic
    assert indice_to_square(0) == 0
    assert indice_to_square(20) == 0
    assert indice_to_square(3) == 1
    assert indice_to_square(23) == 1
    assert indice_to_square(6) == 2
    assert indice_to_square(26) == 2
    assert indice_to_square(27) == 3
    assert indice_to_square(30) == 4
    assert indice_to_square(33) == 5
    assert indice_to_square(80) == 8


def test_reduced_square_to_indices():
    C.set_grid_size(3)  # classic
    assert list(square_to_indices(0)) == [0, 1, 2, 9, 10, 11, 18, 19, 20]
    assert list(square_to_indices(1)) == [3, 4, 5, 12, 13, 14, 21, 22, 23]
    assert list(square_to_indices(8)) == [60, 61, 62, 69, 70, 71, 78, 79, 80]
    assert list(square_to_indices(0, 0, 0)) == [10, 11, 19, 20]
    assert list(square_to_indices(2, 2, 8)) == [6, 7, 15, 16]
    assert list(square_to_indices(4, 3, 3)) == [40, 41, 49, 50]
    assert list(square_to_indices(8, 7, 8)) == [60, 61, 78, 79]
