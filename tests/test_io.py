
from pytest import raises
from random import randrange

from ..parts import Constants as C
from ..parts.io import read_input_stream, string_output


def test_constants_2():
    C.set_grid_size(2)
    assert C.GRID_SIZE == 4
    assert C.GRID_SURFACE == 16
    assert C.SQUARE_LINE_SURFACE == 8
    symbols = C.get_symbols_dict()
    assert symbols['1'] == 0
    assert symbols['4'] == 3
    reverse = C.get_reverse_symbols()
    assert reverse[0] == '1'
    assert reverse[3] == '4'


def test_constants_3():
    C.set_grid_size(3)
    assert C.GRID_SIZE == 9
    assert C.GRID_SURFACE == 81
    assert C.SQUARE_LINE_SURFACE == 27
    symbols = C.get_symbols_dict()
    assert symbols['1'] == 0
    assert symbols['9'] == 8
    reverse = C.get_reverse_symbols()
    assert reverse[0] == '1'
    assert reverse[8] == '9'


def test_constants_4():
    C.set_grid_size(4)
    assert C.GRID_SIZE == 16
    assert C.GRID_SURFACE == 256
    assert C.SQUARE_LINE_SURFACE == 64
    symbols = C.get_symbols_dict()
    assert symbols['1'] == 0
    assert symbols['G'] == 15
    reverse = C.get_reverse_symbols()
    assert reverse[0] == '1'
    assert reverse[15] == 'G'


def test_input():
    C.set_grid_size(3)
    dico = C.get_symbols_dict()
    assert read_input_stream('1234', dico, 4) == [0, 1, 2, 3]
    assert read_input_stream(' 123', dico, 4) == [None, 0, 1, 2]
    assert read_input_stream(' 1\n23', dico, 4) == [None, 0, 1, 2]
    assert read_input_stream(' 789', dico, 4) == [None, 6, 7, 8]


def test_input_raise():
    C.set_grid_size(3)
    dico = C.get_symbols_dict()
    with raises(AssertionError):
        read_input_stream('0123', dico, 5)


def test_output():
    C.set_grid_size(2)
    dico = C.get_reverse_symbols()
    places = [randrange(4) for _ in range(16)]
    out = string_output(places, dico, new_line=False)
    assert len(out) == 16
    out = string_output(places, dico)
    assert len(out) == 19
