
from pytest import raises

from ..parts import Constants as C, NonconsistentGrid
from ..parts.io import read_input_stream
from ..parts.static_classes import AbstractPlaces

GRID_1 = 'x12x3xx1xx1x1xx4'
GRID_2 = 'x42x2xx11x4xx3x4'  # this grid as two '4' in the last square


def places_grid_4():
    C.set_grid_size(2)  # baby
    return read_input_stream(GRID_1, C.SYMBOLS_DICT, C.GRID_SURFACE)


def places_bad_grid_4():
    C.set_grid_size(2)  # baby
    return read_input_stream(GRID_2, C.SYMBOLS_DICT, C.GRID_SURFACE)


def test_abstract_places():
    assert sorted(AbstractPlaces.get_names()) == ['integer', 'set']


def test_init():
    for kind in ('set', 'integer'):
        places = AbstractPlaces.get_class(kind)(places_grid_4())
        assert places.name == kind
        assert places.full_set == places.from_int(0, 1, 2, 3)
        assert places._places[0] is None
        assert places._places[1] == places._from_int(0)


def test_static_methods():
    for kind in ('set', 'integer'):
        cls = AbstractPlaces.get_class(kind)
        assert tuple(cls.iter(cls.from_int(0, 1, 2, 3))) == (0, 1, 2, 3)
        assert tuple(cls.iter(cls.from_int(0, 3))) == (0, 3)
        assert cls.count(cls.from_int(0, 1, 2, 3)) == 4
        assert cls.count(cls.from_int(0, 3)) == 2
        assert tuple(cls.iter(cls.update(cls.from_int(0, 1, 2, 3), 0))) == (1, 2, 3)


def test_values_in_row():
    for kind in ('set', 'integer'):
        places = AbstractPlaces.get_class(kind)(places_grid_4())
        assert places.values_in_row(0) == places.from_int(0, 1)
        assert places.values_in_row(1) == places.from_int(0, 2)
        assert places.values_in_row(2) == places.from_int(0)
        assert places.values_in_row(3) == places.from_int(0, 3)
        for i in range(C.GRID_SIZE):
            assert places.values_in_row(i) == places.rows[i]


def test_values_in_col():
    for kind in ('set', 'integer'):
        places = AbstractPlaces.get_class(kind)(places_grid_4())
        assert places.values_in_col(0) == places.from_int(0, 2)
        assert places.values_in_col(1) == places.from_int(0)
        assert places.values_in_col(2) == places.from_int(0, 1)
        assert places.values_in_col(3) == places.from_int(0, 3)
        for i in range(C.GRID_SIZE):
            assert places.values_in_col(i) == places.cols[i]


def test_values_in_square():
    for kind in ('set', 'integer'):
        places = AbstractPlaces.get_class(kind)(places_grid_4())
        assert places.values_in_square(0) == places.from_int(0, 2)
        assert places.values_in_square(1) == places.from_int(0, 1)
        assert places.values_in_square(2) == places.from_int(0)
        assert places.values_in_square(3) == places.from_int(0, 3)
        for i in range(C.GRID_SIZE):
            assert places.values_in_square(i) == places.squares[i]


def test_compute_allowed_values():
    for kind in ('set', 'integer'):
        places = AbstractPlaces.get_class(kind)(places_grid_4())
        assert places.compute_allowed_values(0) == places.from_int(3)
        assert places.compute_allowed_values(3) == places.from_int(2)
        assert places.compute_allowed_values(5) == places.from_int(1, 3)
        assert places.compute_allowed_values(14) == places.from_int(2)


def test_bad_grid():
    for kind in ('set', 'integer'):
        with raises(NonconsistentGrid):
            AbstractPlaces.get_class(kind)(places_bad_grid_4())


def test_indices_in_subspace():
    places = AbstractPlaces.get_class('integer')(places_grid_4())
    assert tuple(places.indices_in_row(0)) == (0, 3)
    assert tuple(places.indices_in_col(0)) == (0, 8)
    assert tuple(places.indices_in_square(3, -1, -1)) == (11, 14)
    assert tuple(places.indices_in_square(3, 3, 2)) == (11,)
